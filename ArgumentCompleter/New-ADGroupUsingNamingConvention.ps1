function New-ADGroupUsingNamingConvention {
    [CmdletBinding()]
    param (
        [Parameter()]
        [ArgumentCompleter( {
            param ($Command, $Parameter, $WordToComplete, $CommandAst, $FakeBoundParams) # No need to have a line break
            
            Get-Content -Path (Join-Path $PSScriptRoot "Scope.txt") | Where-Object {
                $_ -like "$wordToComplete*"
            } | ForEach-Object {
                "'$_'"
            }
        })]
        [string]$Scope
    )
    
    begin {
        
    }
    
    process {
        $Scope
    }
    
    end {
        
    }
}