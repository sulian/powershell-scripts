
[CmdletBinding()]
param (
)

If ($PSBoundParameters['Debug']) {
    $DebugPreference = 'Continue'
}

[runspacefactory]::CreateRunspacePool()

$SessionState = [System.Management.Automation.Runspaces.InitialSessionState]::CreateDefault()
$RunspacePool = [runspacefactory]::CreateRunspacePool(1, 5)

$PowerShell = [powershell]::Create()
$PowerShell.RunspacePool = $RunspacePool

$RunspacePool.Open()

$Jobs = [System.Collections.ArrayList]::New()

$Script = [System.IO.File]::ReadAllText("D:\Dev\PowerShell\runspace2\file2.ps1")
$Parameters = @{
    Param1 = [string]'Param1'
    Param2 = [string]'Param2'
    Verbose = $true
}

1..30 | ForEach-Object {
    $PowerShell = [powershell]::Create()
    $PowerShell.RunspacePool = $RunspacePool

    [void]$PowerShell.AddScript($Script).AddParameters($Parameters)
    
    $Handle = $PowerShell.BeginInvoke()

    $temp = '' | Select-Object PowerShell,Handle
    $temp.PowerShell = $PowerShell
    $temp.handle = $Handle

    [void]$jobs.Add($Temp)

    Write-Debug ('Available Runspaces in RunspacePool: {0}' -f $RunspacePool.GetAvailableRunspaces())
    Write-Debug ('Remaining Jobs: {0}' -f @($jobs | Where-Object {
        $_.handle.iscompleted -ne 'Completed'
    }).Count)
}

Write-Debug ('Loop finished')

Write-Debug ('Available Runspaces in RunspacePool: {0}' -f $RunspacePool.GetAvailableRunspaces())
Write-Debug ('Remaining Jobs: {0}' -f @($jobs | Where-Object {
    $_.handle.iscompleted -ne 'Completed'
}).Count)

$Return = $Jobs | ForEach-Object {
    $_.powershell.EndInvoke($_.handle)
    $_.PowerShell.Dispose()
}

$Jobs.clear()

$Return | Group-Object ProcessID | Select-Object Count, Name

$Return | Group-Object Thread | Select-Object Count, Name

($Return | Group-Object Thread).Count

$Return | ForEach-Object {
    $_
}

# Write-Verbose 'Test 1'

# [void]$PowerShell.AddScript({
#     param (
#         [string]$LogMsg,
#         [string]$FilePath
#     )

#     [pscustomobject]@{
#         LogMsg = $LogMsg
#         FilePath = $FilePath
#     }
# }).AddParameters($Parameters)



# $AsyncObject = $PowerShell.BeginInvoke()

# $Data = $PowerShell.EndInvoke($AsyncObject)

# $PowerShell.Dispose()

# $Data