param (
    $Param1,
    $Param2
)

$ThreadID = [appdomain]::GetCurrentThreadId()

Write-Verbose 'ThreadID: Beginning $ThreadID' -Verbose

$sleep = Get-Random (1..5)

[pscustomobject]@{
    Param1 = $param1
    Param2 = $param2
    Thread = $ThreadID
    ProcessID = $PID
    SleepTime = $Sleep
}

Start-Sleep -Seconds $sleep

Write-Verbose “ThreadID: Ending $ThreadID” -Verbose
