param(
    [Parameter(Mandatory=$true)]
    [String] $LogMsg,
    [Parameter(Mandatory=$true)]
    [String] $FilePath
)

Start-Sleep -Seconds 4

# Write-Output $LogMsg
[string](Get-Date) + ': ' + $LogMsg | Out-File -FilePath $FilePath -Append
