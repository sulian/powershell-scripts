$MaxRunspaces = 5
$RunspacePool = [runspacefactory]::CreateRunspacePool(1, $MaxRunspaces)
$RunspacePool.Open()

$Jobs = New-Object System.Collections.ArrayList

$Filenames = @("log1.log", "log2.log", "log3.log")

foreach ($File in $Filenames) {
    Write-Host "Creating runspace for $File"
    $PowerShell = [powershell]::Create()
    $PowerShell.RunspacePool = $RunspacePool

    Register-ObjectEvent -InputObject $PowerShell.Streams.Information -EventName DataAdded -Action {
        $recordIndex = $EventArgs.Index
        $data = $PowerShell.Streams.Information[$recordIndex]
        Write-Host "async task wrote '$data'"
    }

    $FilePath = -Join('D:\Dev\PowerShell\runspace1\', $File)

    $Parameters = @{
        LogMsg=  'Log Message goes here'
        FilePath=  $FilePath
    }
    $Script = [System.IO.File]::ReadAllText("D:\Dev\PowerShell\runspace1\file2.ps1")
    [void]$PowerShell.AddScript($Script).AddParameters($Parameters)

    $JobObj = New-Object -TypeName PSObject -Property @{
        Runspace = $PowerShell.BeginInvoke()
        PowerShell = $PowerShell  
    }

    [void]$Jobs.Add($JobObj)
}

$Jobs